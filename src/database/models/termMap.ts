import { Model, DataType, Table, Column, HasOne } from 'sequelize-typescript'
import TermModel from './term'

@Table({
  tableName: 'termMap',
  timestamps: true
})
class TermMapModel extends Model<TermMapModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 0
  })
  termId: number

  @Column({
    type: DataType.INTEGER
  })
  postId: number

  @HasOne(() => TermModel, {
    sourceKey: 'termId',
    foreignKey: 'id',
    constraints: false
  })
  term: TermModel
}

export default TermMapModel
