import { Model, DataType, Table, Column } from 'sequelize-typescript'

@Table({
  tableName: 'comment',
  timestamps: true
})
class CommentModel extends Model<CommentModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.INTEGER
  })
  postId: number

  @Column({
    type: DataType.STRING
  })
  userName: string

  @Column({
    type: DataType.INTEGER
  })
  rating: number

  @Column({
    type: DataType.INTEGER
  })
  avatar: number

  @Column({
    type: DataType.STRING
  })
  message: string
}

export default CommentModel
