import { Model, DataType, Table, Column } from 'sequelize-typescript'

@Table({
  tableName: 'term',
  timestamps: true
})
class TermModel extends Model<TermModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.STRING
  })
  title: string

  @Column({
    type: DataType.STRING,
    defaultValue: 'category', // category,tags
    allowNull: false
  })
  tagType: number
}

export default TermModel
