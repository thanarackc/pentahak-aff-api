import { Model, DataType, Table, Column } from 'sequelize-typescript'

@Table({
  tableName: 'media',
  timestamps: true
})
class MediaModel extends Model<MediaModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.INTEGER
  })
  postId: number

  @Column({
    type: DataType.TEXT
  })
  filePreName: string

  @Column({
    type: DataType.TEXT
  })
  fileName: string

  @Column({
    type: DataType.STRING
  })
  fileExt: string

  @Column({
    type: DataType.STRING
  })
  filePath: string

  @Column({
    type: DataType.TEXT
  })
  fileUrl: string

  @Column({
    type: DataType.STRING
  })
  postType: string

  @Column({
    type: DataType.STRING
  })
  fileSizeName: string
}

export default MediaModel
