import { Model, DataType, Table, Column, HasMany, HasOne } from 'sequelize-typescript'
import CommentModel from './comment'
import TermMapModel from './termMap'
import ReferModel from './refer'
import MediaModel from './media'

@Table({
  tableName: 'posts',
  timestamps: true
})
class PostsModel extends Model<PostsModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  title: string

  @Column({
    type: DataType.STRING
  })
  description: string

  @Column({
    type: DataType.TEXT
  })
  content: string

  @Column({
    type: DataType.STRING,
    defaultValue: 'post', // video,post
    allowNull: false
  })
  postType: number

  @Column({
    type: DataType.SMALLINT,
    defaultValue: 0,
    allowNull: false
  })
  status: number

  @Column({
    type: DataType.STRING
  })
  createdBy: string

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isTrue: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isCredibility: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isItem: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isNewItem: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isMoreFifty: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isFreeTranfer: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isChange: boolean

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false
  })
  isCOD: boolean

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0
  })
  view: number

  @HasMany(() => CommentModel, { foreignKey: 'postId', constraints: false })
  comments: []

  @HasMany(() => TermMapModel, { foreignKey: 'postId', constraints: false })
  terms: []

  @HasMany(() => ReferModel, { foreignKey: 'postId', constraints: false })
  refers: []

  @HasOne(() => MediaModel, { foreignKey: 'postId', constraints: false })
  thumbnail: MediaModel
}

export default PostsModel
