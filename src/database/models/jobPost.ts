import {
  Model,
  DataType,
  Table,
  Column,
  HasMany,
  HasOne
} from 'sequelize-typescript'
import MediaModel from './media'
import PostsModel from './posts'

@Table({
  tableName: 'jobPost',
  timestamps: true
})
class JobPostModel extends Model<JobPostModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.STRING
  })
  uid: string

  @Column({ type: DataType.STRING })
  url: string

  @Column({ type: DataType.STRING })
  title: string

  @Column({ type: DataType.STRING })
  category: string

  @Column({ type: DataType.STRING })
  imageSmall: string

  @Column({ type: DataType.STRING })
  imageLarge: string

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0
  })
  isCurl: number

  @Column({ type: DataType.TEXT })
  content: string

  @Column({ type: DataType.INTEGER })
  wid: string

  @Column({ type: DataType.STRING })
  thumbnailLarge: string

  @Column({ type: DataType.STRING })
  thumbnailSmall: string

  @Column({ type: DataType.STRING })
  remark: string

  @Column({ type: DataType.INTEGER })
  isFacebookShare: number

  @HasMany(() => MediaModel, { foreignKey: 'postId', constraints: false })
  thumbnails: []

  @Column({ type: DataType.STRING })
  source: string

  @HasOne(() => PostsModel, {
    foreignKey: 'id',
    sourceKey: 'wid',
    constraints: false
  })
  postWp: []
}

export default JobPostModel
