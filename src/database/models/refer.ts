import { Model, DataType, Table, Column } from 'sequelize-typescript'

@Table({
  tableName: 'refer',
  timestamps: true
})
class ReferModel extends Model<ReferModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.INTEGER
  })
  postId: number

  @Column({
    type: DataType.STRING
  })
  chanel: number

  @Column({
    type: DataType.TEXT
  })
  url: string
}

export default ReferModel
