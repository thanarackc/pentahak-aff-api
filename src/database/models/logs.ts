import { Model, DataType, Table, Column } from 'sequelize-typescript'

@Table({
  tableName: 'logs',
  timestamps: true
})
class LogsModel extends Model<LogsModel> {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    autoIncrementIdentity: true
  })
  id: number

  @Column({
    type: DataType.STRING
  })
  name: string

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  postId: string

  @Column({
    type: DataType.TEXT
  })
  message: string

  @Column({
    type: DataType.STRING
  })
  createdBy: string
}

export default LogsModel
