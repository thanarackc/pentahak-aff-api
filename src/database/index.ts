import { Sequelize } from 'sequelize-typescript'
import CommentModel from './models/comment'
import LogsModel from './models/logs'
import PostsModel from './models/posts'
import TermModel from './models/term'
import MediaModel from './models/media'
import TermMapModel from './models/termMap'
import ReferModel from './models/refer'
import JobPostModel from './models/jobPost'

const runDatabase = () => {
  // Connect Database
  const sequelize: any = new Sequelize('pentahak', 'admin', 'start0321', {
    host: '206.189.38.110',
    port: 10543,
    dialect: 'postgres',
    logging: true
  })

  // Check connection
  sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been success.')
    })
    .catch((err: any) => {
      console.log(err)
      console.error('Unable to connect database.')
    })

  // Register model
  sequelize.addModels([
    CommentModel,
    LogsModel,
    PostsModel,
    TermModel,
    MediaModel,
    TermMapModel,
    ReferModel,
    JobPostModel
  ])

  // Sync database
  sequelize.sync({ force: false }).then(() => {
    console.log(`Database & tables created!`)
  })

  return sequelize
}

// Export database
export default runDatabase
