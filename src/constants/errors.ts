export const errorCode = {
  errorCode_1: { message: 'Username already !', code: 101 },
  errorCode_2: { message: 'Email already !', code: 102 },
}

export const getError = (errorKey: string) => {
  let value = { message: 'Unknows Error', code: -1 }
  const find = Object.keys(errorCode).findIndex((val) => val === errorKey)
  if (find >= 0) {
    value = Object.values(errorCode)[find]
  }
  return value
}
