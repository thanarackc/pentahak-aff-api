import PostsServices from './posts.service'

// const { FB, FacebookApiException } = require('fb')

const PostsControllers = async (fastify: any, option: any) => {
  // fastify.route({
  //   method: 'POST',
  //   url: '/',
  //   handler: async (request: any, reply: any) => {
  //     const postsServices = new PostsServices()
  //     const result = await postsServices.findALl(request.body.data)
  //     return reply
  //       .code(200)
  //       .send({ message: 'Posts list', result: { data: result } })
  //   },
  // })

  fastify.route({
    method: 'POST',
    url: '/list',
    handler: async (request: any, reply: any) => {
      try {
        const postService = new PostsServices()
        const result = await postService.findAll(request.body.data)
        return reply
          .code(200)
          .send({
            status_code: 200,
            message: 'Get list post.',
            result: { data: result }
          })
      } catch (e) {
        return reply.code(404).send({ status_code: 404, message: e.message })
      }
    }
  })

  fastify.route({
    method: 'GET',
    url: '/:id',
    preHandler: async (request: any, reply: any, done: any) => {
      done()
    },
    handler: async (request: any, reply: any) => {
      try {
        const postService = new PostsServices()
        const result = await postService.findPost(request.params.id)
        return reply.code(200).send({
          status_code: 200,
          message: 'Get post.',
          result: { data: result }
        })
      } catch (e) {
        return reply.code(404).send({ status_code: 404, message: e.message })
      }
    }
  })

  // fastify.route({
  //   method: 'POST',
  //   url: '/save',
  //   preHandler: async (request: any, reply: any) => {
  //     const { checkAuth, token, profile } = await checkTokenFacebook(
  //       request.headers
  //     )
  //     if (checkAuth) {
  //       request.token = token
  //       request.profile = profile
  //     } else {
  //       return false
  //     }
  //     return
  //   },
  //   handler: async (request: any, reply: any) => {
  //     const postService = new PostsServices()
  //     const result = await postService.save(request.profile, request.body.data)
  //     return reply
  //       .code(200)
  //       .send({ message: 'Post saved.', result: { data: result } })
  //   }
  // })

  // fastify.route({
  //   method: 'POST',
  //   url: '/spec',
  //   handler: async (request: any, reply: any) => {
  //     const postSpecService = new PostSpecService()
  //     const result = await postSpecService.find(request.body.data.id)
  //     return reply
  //       .code(200)
  //       .send({ message: 'Spec result.', result: { data: result } })
  //   },
  // })
}

// const checkTokenFacebook = async (headers: any) => {
//   let profile = {}
//   let token = headers.authorization
//   if (token) {
//     token = token.split('Bearer ')
//     if (token[1]) {
//       token = token[1]
//     } else {
//       throw new Error('Token not found')
//     }
//   } else {
//     throw new Error('Token not found')
//   }
//   FB.setAccessToken(token)
//   const tokenVerify = new Promise((resolve, reject) => {
//     FB.api('me?fields=id,name', 'get', {}, (res: any) => {
//       if (!res || res.error) {
//         console.log(!res ? 'error occurred' : res.error)
//         return reject(res.error.message)
//       }
//       profile = res
//       return resolve(true)
//     })
//   })
//   await tokenVerify
//   return { checkAuth: tokenVerify, token, profile }
// }

export default PostsControllers
