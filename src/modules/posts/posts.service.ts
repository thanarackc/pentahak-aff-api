import { Op, Sequelize } from 'sequelize'
import PostsModel from '../../database/models/posts'
import CommentModel from '../../database/models/comment'
import LogsModel from '../../database/models/logs'
import moment from 'moment'
import lodash from 'lodash'
import TermModel from '../../database/models/term'
import TermMapModel from '../../database/models/termMap'
import ReferModel from '../../database/models/refer'
import MediaModel from '../../database/models/media'

class PostsServices {
  postsModel: typeof PostsModel
  commentModel: typeof CommentModel
  logModel: typeof LogsModel
  termModel: typeof TermModel

  constructor() {
    this.postsModel = PostsModel
    this.commentModel = CommentModel
    this.logModel = LogsModel
    this.termModel = TermModel
  }

  async createLog({ name, message, createdBy = null, postId = null }: any) {
    try {
      await this.logModel.create({ name, message, createdBy, postId })
    } catch (e) {
      console.log(e)
      throw new Error(e.message)
    }
  }

  async findAll(option: any) {
    const limit = +option.limit || 20
    const page = +option.page || 1
    let offset = 0
    if (page > 1) {
      offset = limit * page - limit
    }
    let filter: any = {
      where: {}
    }
    if (option.q) {
      filter.include[0].include[0].where = {
        itemName: {
          [Op.iLike]: `%${option.q}%`
        }
      }
    }
    if (
      option.postType &&
      typeof option.postType === 'object' &&
      option.postType.length
    ) {
      filter.where = {
        ...filter.where,
        postType: {
          [Op.in]: option.postType
        }
      }
    }
    if (
      option.server &&
      typeof option.server === 'object' &&
      option.server.length
    ) {
      filter.where = {
        ...filter.where,
        worldServer: {
          [Op.in]: option.server
        }
      }
    }
    if (
      option.location &&
      typeof option.location === 'object' &&
      option.location.length
    ) {
      filter.where = {
        ...filter.where,
        locationMap: {
          [Op.in]: option.location
        }
      }
    }
    if (
      option.status &&
      typeof option.status === 'object' &&
      option.status.length
    ) {
      filter.where = {
        ...filter.where,
        status: {
          [Op.in]: option.status
        }
      }
    }
    // Where rage price
    let setOrder: any = [['createdAt', 'desc']]
    if (option.sort) {
      if (option.sort.id === 1) {
        setOrder = [['id', 'desc']]
      } else if (option.sort.id === 2) {
        setOrder = [['title', 'asc']]
      }
    }
    filter = lodash.set(filter, 'order', setOrder)
    filter = lodash.set(filter, 'limit', limit)
    filter = lodash.set(filter, 'offset', offset)
    filter = lodash.set(filter, 'distinct', 'PostsModel.id')
    // filter = lodash.set(filter, 'subQuery', false)
    // Get result
    const result = await this.postsModel.findAndCountAll(filter)
    const pages = Math.ceil(result.count / limit)
    return {
      data: result.rows,
      total: result.count,
      limit,
      page,
      pages
    }
  }

  // async findAllItems(option: any) {
  //   const limit = +option.limit || 20
  //   const page = +option.page || 1
  //   let offset = 0
  //   if (page > 1) {
  //     offset = limit * page - limit
  //   }
  //   let filter: any = { where: {}, include: [] }
  //   if (option.q) {
  //     filter.where = {
  //       itemName: {
  //         [Op.iLike]: `%${option.q}%`
  //       }
  //     }
  //   }
  //   if (option.postType) {
  //     filter.where = {
  //       ...filter.where,
  //       postType: option.postType
  //     }
  //   }
  //   if (typeof option.itemId === 'object' && option.itemId.length) {
  //     filter.where = {
  //       itemId: {
  //         [Op.in]: option.itemId
  //       }
  //     }
  //   }
  //   // Where rage price
  //   let setOrder: any = [['itemId', 'desc']]
  //   if (option.sort) {
  //     if (option.sort.id === 1) {
  //       setOrder = [['itemId', 'desc']]
  //     }
  //   }
  //   filter = lodash.set(filter, 'order', setOrder)
  //   filter = lodash.set(filter, 'limit', limit)
  //   filter = lodash.set(filter, 'offset', offset)
  //   filter = lodash.set(filter, 'distinct', 'itemsModel.itemId')
  //   // filter = lodash.set(filter, 'subQuery', false)
  //   // Get result
  //   const result = await this.itemsModel.findAndCountAll(filter)
  //   const pages = Math.ceil(result.count / limit)
  //   return {
  //     data: result.rows,
  //     total: result.count,
  //     limit,
  //     page,
  //     pages
  //   }
  // }

  async findPost(id: number) {
    try {
      const post = await this.postsModel.findOne({
        where: { id },
        include: [
          {
            as: 'comments',
            model: CommentModel,
            attributes: ['userName', 'message'],
            separate: true
          },
          {
            as: 'terms',
            model: TermMapModel,
            attributes: ['termId', 'postId'],
            separate: true,
            include: [
              {
                as: 'term',
                model: TermModel,
                attributes: ['title', 'tagType']
              }
            ]
          },
          {
            as: 'refers',
            model: ReferModel,
            attributes: ['chanel', 'url'],
            separate: true
          },
          {
            as: 'thumbnail',
            model: MediaModel,
            attributes: ['fileUrl']
          }
        ]
      })
      if (!post) {
        throw new Error('Post not found.')
      }
      return post
    } catch (e) {
      console.log(e)
      throw new Error(e.message)
    }
  }

  async save(profile: any, data: any) {
    console.log('what?')
    try {
      let logName = 'Create Post'
      let result: any = undefined
      const setData = { ...data }
      delete setData.status
      delete setData.createdAt
      delete setData.updatedAt
      // setData.title = setData.title ? encodeURI(setData.title) : ''
      // setData.characterName = encodeURI(setData.characterName)
      // setData.description = setData.description ? encodeURI(setData.description) : ''
      setData.createdBy = profile.id
      // Check has post by postId key
      if (setData.postType === 'store') {
        setData.expiredAt = moment()
          .add(1, 'd')
          .toDate()
      } else {
        setData.expiredAt = moment()
          .add(7, 'd')
          .toDate()
      }
      if (setData.postId) {
        // Check id match
        const findPost = await this.postsModel.findOne({
          where: { postId: setData.postId },
          attributes: ['createdBy']
        })
        if (findPost && findPost.createdBy !== profile.id) {
          throw new Error(`Can't update post no permission.`)
        }
        logName = 'Update Post'
        result = await this.postsModel.update(setData, {
          where: { postId: setData.postId }
        })
      } else {
        result = await this.postsModel.create(setData)
      }
      if (typeof setData.items === 'object' && setData.items.length) {
        const items: any = []
        setData.items.map((val: any) => {
          const setVal = {
            ...val,
            postId: setData.postId || result.postId,
            ptId: val.ptId ? val.ptId : 0
          }
          items.push(setVal)
        })
        // Loop check have
        for (const item of items) {
          const contition = {
            where: { ptId: item.ptId, postId: setData.postId || 0 }
          }
          const hasItem = await this.commentModel.findOne(contition)
          if (hasItem) {
            delete item.itemName
            delete item.ptCurrency
            delete item.createdAt
            delete item.updatedAt
            await this.commentModel.update(item, contition)
          } else {
            await this.commentModel.create(item)
          }
        }
        // Logs
        await this.createLog({
          name: logName,
          message: `Data: ${JSON.stringify(
            setData
          )}, DataAfter: ${JSON.stringify(result)}`,
          createdBy: setData.createdBy,
          postId: setData.postId || result.postId || ''
        })
      }
      return result
    } catch (e) {
      console.log(e)
      throw new Error(e.message)
    }
  }
}

export default PostsServices
