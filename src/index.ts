import sequelize from './database'
import { PostsControllers } from './modules'

const path = require('path')
const fastifySession = require('fastify-session')
const fastifyCookie = require('fastify-cookie')
const fastifyFormbody = require('fastify-formbody')
const fastifyCors = require('fastify-cors')
const fastifyStatic = require('fastify-static')
const fastifyJwt = require('fastify-jwt')
const fastify = require('fastify')({
  // logger: true
})

// Get env
if (process.env.NODE_ENV === 'production') {
  require('dotenv').config({ path: '.env.prod' })
} else {
  require('dotenv').config({ path: '.env.dev' })
}

async function start() {
  try {
    // Config server
    const config = {
      host: process.env.HOST || '127.0.0.1',
      port: process.env.PORT
    }

    // Database run
    sequelize()

    // Cors
    fastify.register(fastifyCors, {
      // origin: [process.env.DOMAIN, process.env.DOMAIN_BACKEND]
      origin: '*'
    })

    // Static file
    fastify.register(fastifyStatic, {
      root: path.join(__dirname, '../', 'static'),
      prefix: '/static/'
    })

    // Register form body &&  Set Cookie
    fastify.register(fastifyJwt, {
      secret: 'storefast-app-token'
    })
    fastify.register(fastifyFormbody)
    fastify.register(fastifyCookie)
    fastify.register(fastifySession, {
      cookieName: 'storefast-sessionId',
      secret: 'L96EzuM9Bn_Np#@T@KX$QhEee@kN7*6U',
      cookie: { secure: false },
      expires: 1800000
    })

    // Api path
    fastify.register(PostsControllers, { prefix: '/api/v1/posts' })

    // Start server
    fastify.listen(config.port, config.host, (err: any, address: any) => {
      if (err) {
        fastify.log.error(err)
        process.exit(1)
      }
    })
  } catch (e) {
    console.log(e)
  }
}

start()
